#include <staticjson/staticjson.hpp>

using namespace staticjson;

struct MyObject {
    int i;

    void staticjson_init(ObjectHandler* h) {
        h->set_flags(Flags::DisallowUnknownKey);
        h->add_property("i", &i);
    }
};

int main() {
    MyObject obj;
    const char* input = "{\"i\": -980008}";
    from_json_string(input, &obj, nullptr);
    return obj.i == -980008 ? 0 : 1;
}
