import os
import shutil
from conans import ConanFile, CMake
from conans.tools import download, unzip
from contextlib import contextmanager


@contextmanager
def temp_dir(name):
    cur = os.getcwd()
    os.mkdir(name)
    os.chdir(name)
    yield
    os.chdir(cur)


class StaticJSONConan(ConanFile):
    name = "StaticJSON"
    version = "0.4.1"
    url = "https://gitlab.com/cinghiale/conan-staticjson"
    license = "The MIT License (MIT)"
    settings = "os", "compiler", "build_type", "arch"

    options = {"fPIC": [True, False]}
    default_options = "fPIC=True"
    exports = ("CMakeLists.txt",)

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.remove("fPIC")

    def source(self):
        url = "https://github.com/netheril96/StaticJSON/archive/0.4.1.tar.gz"
        archive = "staticjson.tar.gz"
        download(url, archive)
        unzip(archive)
        shutil.move("StaticJSON-0.4.1", "StaticJSON")
        os.unlink(archive)

    def build(self):
        shutil.copy("CMakeLists.txt", "StaticJSON/CMakeLists.txt")
        cmake = CMake(self.settings)

        flags = []
        if self.options.fPIC:
            flags.append("-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE")

        if self.scope.dev:
            with temp_dir("build_test"):
                self.run('cmake ../StaticJSON/test %s %s' % (cmake.command_line, " ".join(flags)))
                self.run("cmake --build . %s" % cmake.build_config)
                self.run("ctest")

        with temp_dir("build"):
            self.run('cmake ../StaticJSON %s %s' % (cmake.command_line, " ".join(flags)))
            self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h*", dst="include", src="StaticJSON/include")
        self.copy("*.lib", dst="lib", src="build/")
        self.copy("*.a", dst="lib", src="build/")

    def package_info(self):
        self.cpp_info.cppflags = ["-std=c++11"]
        self.cpp_info.libs = ["StaticJSON"]
